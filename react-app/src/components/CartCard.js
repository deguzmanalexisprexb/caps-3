import { Container, Row, Col, Carousel, img, Button, Form } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

import Swal2 from 'sweetalert2';

const CartCard = ({prop, refreshCart}) => {

	// Prop passing
	const { productId, quantity } = prop;

	// Get token in local storage
	const token = localStorage.getItem('token');

	// Store useNavigate function to navigate variable
	const navigate = useNavigate();

	// Initialize states
	const [ productName, setProductName ] = useState('');
	const [ productPrice, setProductPrice ] = useState(0);
	const [ imagesUrl, setImagesUrl ] = useState([]);

	// Define Total Amount for each order
	const totalAmount = productPrice * quantity;

	// Delete items in cart function
	const removeFromCart = productId => {
		Swal2.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
	  		if (result.isConfirmed) {
	  			fetch(`${process.env.REACT_APP_API_URL}/orders/${productId}/removeProducts`,{
	  				method: 'PATCH',
	  				headers: { Authorization : `Bearer ${token}`}
	  			})
	  			.then(res => res.json())
	  			.then(data => {
	  				if ( !data ) {
	  				    Swal2.fire({
	  				        title: 'Item Delete unsuccessful',
	  				        icon: 'error',
	  				        text: 'Check your cart and try again'
	  				    })
	  				} else {
	  				    Swal2.fire({
	  				        title: 'Item Delete successful',
	  				        icon: 'success',
	  				        text: 'Item Deleted'
	  				    })
	  				    refreshCart();
	  				}
	  			})
	  		}
		})
	}

	const updateQuantity = productId => {
		Swal2.fire({
		  title: 'Quantity',
		  icon: 'info',
		  input: 'range',
		  inputLabel: 'quantity',
		  showCloseButton: true,
		  inputAttributes: {
		    min: 1,
		    max: 25,
		    step: 1
		  },
		  inputValue: quantity
		}).then( result => {
			const newQuantity = result.value;
			fetch(`${process.env.REACT_APP_API_URL}/orders/changeQuantity`, {
				method: 'PATCH',
				headers: { Authorization : `Bearer ${token}`, 'Content-Type': 'application/json'},
				body: JSON.stringify({
					productId: productId,
					quantity:  newQuantity
				})
			})
			.then(res => res.json())
			.then(data => {
				Swal2.fire({
			        title: 'Quantity update successful',
			        icon: 'success',
			        text: 'Quantity Updated'
			    })
			    refreshCart();
			})
		})
	}

	// Get the product name and price from backend API
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setProductName(data.name);
			setProductPrice(data.price);
			setImagesUrl(data.imagesUrl);
		})
	}, [])

	// checker
	/*useEffect(() => {
		console.log(imagesUrl);
	}, [imagesUrl])*/

	return (
		<Row className='my-4'>
			<Col lg={2} className="p-3">
				<Carousel id="carousel2" interval={2000}>
					{
                        imagesUrl.map(image => (
                            <Carousel.Item key={image.public_id}>
                                <img className="d-block w-100" src={image.url} alt={`Slide ${image.public_id}`} />
                            </Carousel.Item>
                        ))  
                    }
				</Carousel>
			</Col>
        	<Col lg={8} className="d-flex flex-column justify-content-center">
         	<Form /*onSubmit={addToCart}*/>
					<h1>{productName}</h1>
					<p>&#8369; {productPrice}.00/pc</p>
					<Form.Group>
         	   	<div className="d-flex align-items-start">
         	   		<Form.Label className='me-2'>Quantity: {quantity}pcs</Form.Label>
         	   	</div>
         		</Form.Group>
         		<div className="pt-2">
         	   	<p>
         	      	TOTAL: &#8369; {totalAmount}.00/pc
         	   	</p>
         		</div>
         	</Form>
        	</Col>
        	<Col lg={2} className="d-flex flex-row flex-md-column justify-content-center">
    			<div className="d-inline-block my-2">
    		   		<Button variant="info" onClick={() => updateQuantity(productId)}>Update	</Button>
    			</div>
     			<div className="d-inline-block my-2">
     		   		<Button variant="danger" onClick={() => removeFromCart(productId)}>Delete</Button>
     			</div>
        	</Col>
     	</Row>
	);
};

export default CartCard;