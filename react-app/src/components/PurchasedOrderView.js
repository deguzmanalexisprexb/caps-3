import { Container, Row, Col, Carousel, img, Button, Form } from 'react-bootstrap';
import { useEffect, useState } from 'react';
import { useParams, Link } from 'react-router-dom';

// Import components
import BackButton from '../components/BackButton';
import PurchasedOrderViewCard from './PurchasedOrderViewCard';


const PurchasedOrderView = () => {

	// Initializations
	const [ error, setError ] = useState();
	const [ isError, setIsError ] = useState(false);
	const [ singlePurchase, setSinglePurchase ] = useState([]);
	const [ totalAmount, setTotalAmount ] = useState();
	const [ purchasedOn, setPurchaseOn ] = useState();

	// Gets the id in url
	const { userId,purchaseOrderId } = useParams();

	// Get token in local storage
	let token = localStorage.getItem('token');

	// Convert the purchasedOn date string to Date object
	const purchaseDate = new Date(purchasedOn);

	// Format the date as month/day/year
	const formattedPurchaseDate = purchaseDate.toLocaleDateString('en-US', {
		month: 'short',
		day: 'numeric',
		year: 'numeric',
	});

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/purchasedOrders/singlePurchase/${userId}/${purchaseOrderId}`, {
			headers: { Authorization: `Bearer ${token}` }
		})
		.then(res => {
			if (res.ok) {
			    setIsError(false)
			    return res.json();
			} else {
			    res.text().then((message) => {
			        setIsError(true)
			        setError(message);
			    })
			}
		})
		.then(data => {
			setSinglePurchase(data.products.map(product => {
				return (
					<PurchasedOrderViewCard key={product._id} product={product} />
				)
			}))
			setPurchaseOn(data.purchasedOn);
			setTotalAmount(data.totalAmount);
		})
	}, [])

    return (
		<div 
			className="py-5 mt-5 min-vh-100"
			style={{ 
				backgroundImage: 'url("https://images.pexels.com/photos/3527786/pexels-photo-3527786.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2")', 
				backgroundPosition: 'right bottom', 
				backgroundSize: 'cover', 
				backgroundRepeat: 'repeat', 
				backgroundAttachment: 'fixed',
				background: 'linear-gradient(to bottom, rgba(255, 255, 255, 0.3), rgba(255, 255, 255, 0.5)), url("https://images.pexels.com/photos/3527786/pexels-photo-3527786.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2")' 
			}}
		>
			<Container className="pb-5">
		 		<Row>
			      	<Col lg={12}>
			         	<BackButton />
			      	</Col>
			      	<Col lg={12} className='text-center'>
			         	<h1 className='pageTitle display-4'>A Purchase of Beauty, Captured in Resin</h1>
			         	<h3>on {formattedPurchaseDate}</h3>
			      	</Col>
			      	{
			      		isError
			      		&&
		      			<Col lg={12} className='text-center'>
		      		   		<h2>{error}</h2>
		      			</Col>
			      	}
			      	
		      	</Row>
		     		{singlePurchase}
		 	</Container>
		 	<Container  className="text-center">
		 		<div className="
	 				py-0 
		 			fixed-bottom 
		 			d-flex flex-column 
		 			flex-md-row 
		 			justify-content-center
		 			justify-content-md-end
		 			align-items-center
		 			pe-md-5
	 			"
		 		style={{backgroundImage: 'linear-gradient(to left, #C55FFB, #EFDCF9)'}}>
			 		<div className='px-4'>
						<h2>Total Amount:  &#8369; {totalAmount}.00</h2>
					</div>
					<div className="d-inline my-2">
						<Button as={Link} to='/purchasedOrders' variant="success">Order this again</Button>
				 	</div>
	 	 		</div>
		 	</Container>
	 	</div>
    );
};

export default PurchasedOrderView;