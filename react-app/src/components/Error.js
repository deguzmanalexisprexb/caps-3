import { useState } from 'react';
import { Form } from 'react-bootstrap';

const Error = ({error, isError}) => {
    return (
    	<>
    	{
    		isError
    		&& 
    		<Form.Text className="text-danger">{error}</Form.Text> 
    	}
    	</>
    );
};

export default Error;