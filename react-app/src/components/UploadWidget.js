import { useState, useEffect, useRef } from 'react';
import { Button } from 'react-bootstrap';
import axios from 'axios';

import { AiFillCloseCircle } from 'react-icons/ai';
import { MdAddPhotoAlternate } from 'react-icons/md';


const UploadWidget = ({setImagesUrl, imagesUrl}) => {

	/*const [ images, setImages ] = useState([]);*/
	const [ imageToRemove, setImageToRemove ] = useState(null);

	function handleRemoveImg(imgObj){
		setImageToRemove(imgObj.public_id);
		axios.delete(`${process.env.REACT_APP_API_URL}/${imgObj.public_id}`)
		.then( () => {
			setImageToRemove(null);
			/*setImages(prev => prev.filter(img => img.public_id !== imgObj.public_id));*/
			setImagesUrl(prev => prev.filter(img => img.public_id !== imgObj.public_id));
		})
		.catch(e => console.log(e));
	}

	function handleOpenWidget() {
		var myWidget = window.cloudinary.createUploadWidget(
			{
				cloudName: 'dvcnii6bx',
				uploadPreset: 'psvv6mm4',
				secure: true
			}, 
			(error, result) => {
				if(!error && result && result.event === 'success'){
					console.log("Done! Here is the image info: ", result.info);
					/*setImages(prev => [...prev, {url: result.info.url, public_id: result.info.public_id}])*/
					setImagesUrl((prev) => [...prev, {url: result.info.url, public_id: result.info.public_id}]);
				}
			}
		);
		console.log("hi");
		myWidget.open()
	}

	/*useEffect(() => {
	  console.log(`images ${images}`);
	}, [images]);*/

    return (
    	<>
    		<div className="d-flex flex-column align-items-center justify-content-center mt-3">
    			{imagesUrl.map((image) => (
    			  <div key={image.public_id} style={{ width: '200px', height: '200px' }} className='position-relative'>
    			    <img src={image.url} alt="Uploaded Image" style={{ width: '100%', height: '100%' }}/>
    			  	{
    			  		imageToRemove != image.public_id 
    			  		&&
		  		      	<button
		  					className={`position-absolute top-0 end-0 bg-transparent border-0 p-2`}
		  					onClick={() => handleRemoveImg(image)}
		  				>
		  					<AiFillCloseCircle size={30} className='text-danger' /> :
		  				</button>
    			  	}
    			  </div>
    			))}
    		</div>
    		<Button onClick={() => handleOpenWidget()}>
				<MdAddPhotoAlternate size={30} /> Upload Product Image
			</Button>
    	</>
    	
    );
};

export default UploadWidget;