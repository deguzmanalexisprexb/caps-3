import { Container, Row, Col, Carousel } from 'react-bootstrap';

const Story = () => {
    return (
	     	<Row>
				<Col lg={6} className="p-3 mx-auto">
					<Carousel fade id="carousel2" interval={3000}>
							<Carousel.Item>
							   	<img className="d-block w-100" src="https://res.cloudinary.com/dvcnii6bx/image/upload/v1690186144/covers/336875565_601519918516386_3136588216423922350_n_nhyhvi.jpg" />
							</Carousel.Item>
							<Carousel.Item>
							   	<img className="d-block w-100" src="https://res.cloudinary.com/dvcnii6bx/image/upload/v1690186143/covers/336860692_527433046238244_7920680313975262090_n_bmkfii.jpg" />
							</Carousel.Item>
							<Carousel.Item>
							   	<img className="d-block w-100" src="https://res.cloudinary.com/dvcnii6bx/image/upload/v1690186144/covers/336895028_932088564642677_6545047639312034969_n_wtp0pw.jpg" />
							</Carousel.Item>
							<Carousel.Item>
							   	<img className="d-block w-100" src="https://res.cloudinary.com/dvcnii6bx/image/upload/v1690186143/covers/336693132_207326431902704_2508804438338343572_n_gytrh0.jpg" />
							</Carousel.Item>
							<Carousel.Item>
							   	<img className="d-block w-100" src="https://res.cloudinary.com/dvcnii6bx/image/upload/v1690186144/covers/337561893_964947714665008_6536970412882000704_n_ylimax.jpg" />
							</Carousel.Item>
							<Carousel.Item>
							   	<img className="d-block w-100" src="https://res.cloudinary.com/dvcnii6bx/image/upload/v1690186143/covers/336720517_3390831411154847_6091604979943601692_n_dsuvom.jpg" />
							</Carousel.Item>
					</Carousel>
				</Col>
	     	</Row>		
    );
};

export default Story;		