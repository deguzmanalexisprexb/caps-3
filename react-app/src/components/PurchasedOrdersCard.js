import { Container, Row, Col, Carousel, img, Button, Form, Modal } from 'react-bootstrap';
import { Link } from 'react-router-dom';


const PurchasedOrdersCard = ({ order, refreshOrders }) => {

	const { _id: id, userId, products, totalAmount, isPaid, isDelivered, purchasedOn } = order;

	// Convert the purchasedOn date string to Date object
	const purchaseDate = new Date(purchasedOn);

	// Format the date as month/day/year
	const formattedPurchaseDate = purchaseDate.toLocaleDateString('en-US', {
		month: 'short',
		day: 'numeric',
		year: 'numeric',
	});

    return (
    				<Row 
    	                className='my-4 rounded border mx-auto' 
    	                style={{ 
    	                    /*backgroundImage: 'url("https://images.pexels.com/photos/4175070/pexels-photo-4175070.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2")',
    	                    backgroundPosition: 'right bottom', 
    	                    backgroundSize: 'cover', 
    	                    backgroundRepeat: 'repeat', 
    	                    backgroundAttachment: 'fixed',
    	                    maxWidth: '800px',*/
    	                    background: '#EFDCF9'
    	                }}
    	            >
	    	            <Col lg={10} className="d-flex flex-column justify-content-center">
							<Row>
		    	                <Col xs={12} md={5}>
									<p className='mb-0'><strong>Purchase Date:</strong> {formattedPurchaseDate}</p>
									<p className='mb-0'><strong>Total Items:</strong> {products.length}</p>
									<p className='mb-0'><strong>Total Amount:</strong> &#8369; {totalAmount}</p>
		    	                </Col>
		    	                <Col xs={12} md={7}>
									<p className='mb-0'><strong>Order Id:</strong> {id}</p>
									<strong>Payment:</strong>
									{isPaid ? <span>paid</span> : <span> not yet paid</span>}
									<p className='mb-0'>
										<strong>Delivery Status:</strong> 
										{
											isDelivered 
											? 
											<span> delivered</span> 
											: 
											<span> not yet delivered</span>
										}
									</p>
		    	                </Col>
							</Row>
	    	            </Col>
    		        	<Col lg={2} className="d-flex flex-row flex-md-column justify-content-center">
    	                    <div className="d-inline-block my-2">
    	                        <Button 
    	                        	as={Link} 
    	                        	variant="info" 
    	                        	to={`/purchasedOrders/singlePurchase/${userId}/${id}`} 
    	                        	style={{ background: '#EFDCF9'}}
    	                        	className='border-0'
    	                        >
    	                        	See More
    	                        </Button>
    	                    </div>

    		        	</Col>
    		     	</Row>
    );
};

export default PurchasedOrdersCard;