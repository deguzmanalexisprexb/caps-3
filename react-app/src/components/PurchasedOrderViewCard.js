import { Container, Row, Col, Carousel, img, Button, Form } from 'react-bootstrap';
import { useState, useEffect } from 'react';

const PurchasedOrderViewCard = ({ product }) => {

	// Prop passing
	const { productId, quantity } = product;

	// Initialization
	const [ productName, setProductName ] = useState('');
	const [ productPrice, setProductPrice ] = useState('');
	const totalAmount = productPrice * quantity;
	const [ imagesUrl, setImagesUrl ] = useState([]); 

	// Get the product name and price from backend API
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setProductName(data.name);
			setProductPrice(data.price);
			setImagesUrl(data.imagesUrl);
		})
	}, [])

    return (
		<Row className='my-4'>
			<Col lg={3} className="p-3">
				<Carousel id="carousel2" interval={5000}>
					{
						imagesUrl.map(image => (
							<Carousel.Item key={image.public_id}>
							   	<img className="d-block w-100" src={image.url} alt={`Slide ${image.public_id}`} />
							</Carousel.Item>
						))	
					}
				</Carousel>
			</Col>
        	<Col lg={9} className="d-flex flex-column justify-content-center">
         	<Form /*onSubmit={addToCart}*/>
					<h1>{productName}</h1>
					<p>&#8369; {productPrice}.00/pc</p>
					<Form.Group>
         	   	<div className="d-flex align-items-start">
         	   		<Form.Label className='me-2'>Quantity: {quantity}pcs</Form.Label>
         	   	</div>
         		</Form.Group>
         		<div className="pt-2">
         	   	<p>
         	      	TOTAL: &#8369; {totalAmount}.00/pc
         	   	</p>
         		</div>
         	</Form>
        	</Col>
     	</Row>
    );
};

export default PurchasedOrderViewCard;