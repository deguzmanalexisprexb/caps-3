import { useNavigate } from 'react-router-dom';

// import icons
import { BsFillArrowLeftCircleFill } from 'react-icons/bs';

const BackButton = () => {
	const navigate = useNavigate();

	// go to previos page function
	const goBack = () => {
	  navigate(-1);
	};

    return (
    	<BsFillArrowLeftCircleFill onClick={goBack} size={40} style={{ color: '#C55FFB', cursor: 'pointer' }} />
    );
};

export default BackButton;