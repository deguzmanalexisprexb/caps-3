import { Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useState } from 'react';

// Import icons
import { AiOutlineHeart, AiFillHeart } from 'react-icons/ai';
import { BsCartPlus } from 'react-icons/bs';

const ProductCard = ({productProp}) => {

	const { _id, imagesUrl, name, description, price } = productProp;

	// Initialize states
	const [isLiked, setIsLiked] = useState(false);

	// Handle Like function
	const handleLike = () => {
	  	setIsLiked(prevState => !prevState);
	};

	return (
		<Col 
			xs={6} 
			md={3} 
			className="p-3" 
			/*style={{backgroundImage: 'linear-gradient(to top, rgba(255, 100, 255, 0.3), rgba(255, 255, 255, 0.5))'}}*/
		>
			<Card className="text-center h-100">
		      	<Card.Img variant="top" src={imagesUrl[0].url} alt={imagesUrl[0].public_id} />
		      	<button
					className={`position-absolute top-0 end-0 bg-transparent border-0 p-2`}
					onClick={handleLike}
				>
					{isLiked ? <AiFillHeart size={20} className='text-danger' /> : <AiOutlineHeart size={20} />}
				</button>
				<button className={`position-absolute top-0 end-0 bg-transparent border-0 p-2 mt-4`}>
					<BsCartPlus size={20} />
				</button>
		      	<Card.Body className='d-flex flex-column'>
		      		<div className='mb-3'>
		      			<Card.Title><b>{name}</b></Card.Title>
		      		</div>
					<div className='mt-auto'>
						<Card.Text>&#8369; {price}.00</Card.Text>
						<Button as={Link} style={{backgroundColor: '#C55FFB'}} to={`/products/${_id}`} className='text-white border-0'>More details</Button>
					</div>
		      	</Card.Body>
			</Card>
		</Col>
	);
};

export default ProductCard;