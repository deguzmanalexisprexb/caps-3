import { Row, Col } from 'react-bootstrap';
import { useState, useEffect } from 'react';

const CheckoutCard = ({prop}) => {

	// Prop passing
	const { productId, quantity } = prop;

	// Initialize states
	const [ productName, setProductName ] = useState('');
	const [ productPrice, setProductPrice ] = useState(0);

	// Initialize totalAmount
	const totalAmount = productPrice * quantity;

	// Get the product name and price from backend API
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setProductName(data.name);
			setProductPrice(data.price);
		})
	}, [])

    return (
        <Row className="d-flex justify-content-between border-bottom">
			<Col className="mb-1 col-lg-2">{quantity} pcs</Col>
			<Col className="mb-1 col-lg-4">{productName}</Col>
			<Col className="mb-1 col-lg-3">&#8369;{productPrice}</Col>
			<Col className="mb-1 fw-bold col-lg-3">&#8369;{totalAmount.toFixed(2)}</Col>
		</Row>
    );
};

export default CheckoutCard;