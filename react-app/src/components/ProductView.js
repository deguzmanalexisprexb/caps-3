import { Container, Row, Col, Carousel, img, Button, Form } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { useParams, useNavigate } from 'react-router-dom';

// import components
import BackButton from '../components/BackButton';
import UserContext from '../UserContext';
import { AiOutlineMinusCircle, AiOutlinePlusCircle } from 'react-icons/ai';

import Swal2 from 'sweetalert2';


const ProductView = () => {

	// States
	const [ productName, setProductName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState('');
	const [ quantity, setQuantity ] = useState(1);
	const [ error, setError ] = useState(false);
	const [ images, setImages ] = useState([]);

	// Inititalize totalAmount
	const totalAmount = price * quantity;

	// Gets the id in url
	const { id } = useParams();

	// User context
	const{ user } = useContext(UserContext);

	// get specific product detail from backend API
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
		.then(res => res.json())
		.then(data => {
			setImages(data.imagesUrl)
			setProductName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [])

	// Set quantity on change
	const handleQuantityChange = (event) => {
		const value = parseInt(event.target.value, 10);
		setQuantity(value);
	};

	const addToCart = (e) => {
		e.preventDefault();
		let token = localStorage.getItem('token');

		if ( token ){
			fetch(`${process.env.REACT_APP_API_URL}/orders/addToCart`, {
				method: 'PATCH',
				headers: { 
					'Content-Type': 'application/json',
					Authorization: `Bearer ${token}`
				},
				body: JSON.stringify({
					productId: id,
					quantity: quantity
				})
			})
			.then(res => {
				if (res.ok) {
					return res.json();
				} else {
					res.text().then((message) => {
						setError(message);
					})

				}
			})
			.then(data => {
				if ( !data ) {
					Swal2.fire({
			            title: "Add to cart unsuccessful!",
			            icon: 'error',
			            text: 'Check your cart and try again'
	      			})
				} else {
					Swal2.fire({
						position: 'top',
						icon: 'success',
						title: 'Your item has been added to Cart',
						showConfirmButton: false,
						timer: 1000
					})
				}
			})
		} else {
			Swal2.fire({
            title: "Add to cart unsuccessful!",
            icon: 'error',
            text: 'You are not logged in'
   		})
		}

	}

	// Checker
	/*useEffect(()=> {
		console.log(images);
	}, [images])*/

	return (
		<div 
			className="py-4 mt-5" 
			style={{ 
				backgroundImage: 'url("https://images.pexels.com/photos/3527786/pexels-photo-3527786.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2")', 
				backgroundPosition: 'right bottom', 
				backgroundSize: 'cover', 
				backgroundRepeat: 'repeat', 
				background: 'linear-gradient(to bottom, rgba(255, 255, 255, 0.3), rgba(255, 255, 255, 0.5)), url("https://images.pexels.com/photos/3527786/pexels-photo-3527786.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2")',
				backgroundAttachment: 'fixed', 
			}}
		>
		 	<Container className="pb-5 min-vh-100">
		 		<Row>
		      	<Col lg={12}>
	            	<BackButton />
		      	</Col>
		      	</Row>
		     	<Row>
					<Col lg={5} className="p-3">
						<Carousel id="carousel2" interval={3000}>
						{
							images.map(image => (
								<Carousel.Item key={image.public_id}>
								   	<img className="d-block w-100" src={image.url} alt={`Slide ${image.public_id}`} />
								</Carousel.Item>
							))	
						}
						</Carousel>
					</Col>
		        	<Col lg={7} className="p-md-5 p-3 d-flex flex-column justify-content-center">
	            	<Form onSubmit={addToCart}>
							<h1>{productName}</h1>
							<p>{description}</p>
							<p>&#8369; {price}.00/pc</p>
						<Form.Group>
		            	   	<div className="d-flex align-items-start">
		            	   		<Form.Label className='me-2' htmlFor="quantity">Quantity:</Form.Label>
		            	   		<Button className="ms-2" style={{ border: 'none', background: 'none', padding: '0', cursor: 'pointer' }}>
									<AiOutlineMinusCircle
										className='me-2'
		            	      	        size={25}
		            	      	        style={{ cursor: 'pointer' }}
		            	      	        onClick={() => quantity > 1 && setQuantity(quantity - 1)}
									/>
	            	      	    </Button>
		            	   		<Form.Control 
										type="number" 
										min="1" 
										style={{ width: '60px', height: '25px' }} 
										value={quantity} 
										onChange={handleQuantityChange} 
										onKeyDown={(e) => e.keyCode === 48 && e.preventDefault()}
										id="quantity"
		            	      	/>
		            	      	<Button className="ms-2" style={{ border: 'none', background: 'none', padding: '0', cursor: 'pointer' }}>
									<AiOutlinePlusCircle
		            	      	        size={25}
		            	      	        style={{ cursor: 'pointer' }}
		            	      	        onClick={() => setQuantity(quantity + 1)}
									/>
	            	      	    </Button>
		            	   	</div>
	            		</Form.Group>
	            		<div className="pt-2">
	            	   	<p>
	            	      	TOTAL: &#8369; {totalAmount}.00/pc
	            	   	</p>
	            	   	<div className="d-inline-block">
	            	   		{user.isAdmin && <p className='text-danger'>Admins are not allowed to add to Cart</p>}
	            	      	<Button variant="primary" type="submit" disabled={user.isAdmin}>Add to cart</Button>
	            	   	</div>
	            		</div>
	            	</Form>
		        	</Col>
		     	</Row>
		 	</Container>
    	</div>
	);
};

export default ProductView;