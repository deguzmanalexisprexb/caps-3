import { Container, Row, Col, Button } from 'react-bootstrap';

import Story from '../components/Story';

const Home = () => {
  return (
    <div 
      className="py-5 text-center min-vh-100" 
      style={{ 
        backgroundImage: 'url("https://res.cloudinary.com/dvcnii6bx/image/upload/v1690184816/covers/cover_rq0mtg.png")',
        backgroundPosition: 'bottom',
        backgroundSize: 'cover',
        backgroundRepeat: 'repeat',
        background: 'linear-gradient(to bottom, rgba(255, 255, 255, 0.3), rgba(255, 255, 255, 0.5)), url("https://res.cloudinary.com/dvcnii6bx/image/upload/v1690184816/covers/cover_rq0mtg.png")',
        backgroundAttachment: 'fixed' 
      }}       
    >
      
      <Container className="py-5">
        <Row>
          <Col lg={8} md={10} className="mx-auto">
            <h1 className="pageTitle mb-4 display-3">Resin Magic</h1>
            <p className="lead display-6">Transforming Moments into Masterpieces</p>
          </Col>
        </Row>
        <Story />
        
      </Container>
    </div>

  );
};

export default Home;