import { Container, Row, Col } from 'react-bootstrap';
import { useState, useEffect } from 'react';

// import components
import ProductCard from '../components/ProductCard';
import BackButton from '../components/BackButton';

const Products = () => {

  const [ products, setProducts ] = useState([]);
  const [ isPending, setIsPending ] = useState(true);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/allActive`)
      .then((res) => res.json())
      .then((data) => {
        // Sort the products array alphabetically by product name
        const sortedProducts = data.sort((a, b) =>
          a.name.localeCompare(b.name)
        );

        setProducts(
          sortedProducts.map((product) => (
            <ProductCard key={product._id} productProp={product} />
          ))
        );

        setIsPending(false);
      });
  }, []);

  return (
    <div 
      className="py-4 mt-5" 
      style={{ 
        backgroundImage: 'url("https://images.pexels.com/photos/3527786/pexels-photo-3527786.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2")', 
        backgroundPosition: 'right bottom', 
        backgroundSize: 'cover', 
        backgroundRepeat: 'repeat', 
        background: 'linear-gradient(to bottom, rgba(255, 255, 255, 0.3), rgba(255, 255, 255, 0.5)), url("https://images.pexels.com/photos/3527786/pexels-photo-3527786.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2")',
        backgroundAttachment: 'fixed',
      }}
    >
      <Container className='min-vh-100'>
        <Row className='text-black' >
          <Col lg={12}>
            <BackButton />
          </Col>
          <Col md={8} className="mx-auto text-center" >
            <h1 className='pageTitle fw-bold display-3'>Artistic Creations</h1>
            <p className="mb-4">Discover our Collection of Masterpieces</p>
          </Col>
        </Row>
        { isPending && <div>Loading...</div>}
        <Row>
          {products}
        </Row>
      </Container>
    </div>
  );
};

export default Products;