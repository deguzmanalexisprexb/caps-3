import { useState } from 'react';
import { Container, Row, Col, Nav } from 'react-bootstrap';

import AdminProductSetup from '../adminComponents/AdminProductSetup';
import AdminOrderSetup from '../adminComponents/AdminOrderSetup';
import AdminUserSetup from '../adminComponents/AdminUserSetup';

const AdminDashboard = () => {
  const [activeTab, setActiveTab] = useState('tabone');

  const handleTabChange = (tab) => {
    setActiveTab(tab);
  };

  return (
    <div className="py-5 my-5">
      <Container fluid className='px-4'>
        <Row>
          <Col>
            <Nav variant="tabs" defaultActiveKey="tabone">
              <Nav.Item>
                <Nav.Link
                  eventKey="tabone"
                  active={activeTab === 'tabone'}
                  onClick={() => handleTabChange('tabone')}
                  style={activeTab === 'tabone' ? {backgroundColor: '#C55FFB', color: '#EFDCF9'} : {color: '#C55FFB'}}
                >
                  <i className="fa fa-home"></i> PRODUCTS
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link
                  eventKey="tabtwo"
                  active={activeTab === 'tabtwo'}
                  onClick={() => handleTabChange('tabtwo')}
                  style={activeTab === 'tabtwo' ? {backgroundColor: '#C55FFB', color: '#EFDCF9'} : {color: '#C55FFB'}}
                >
                  <i className="fa fa-bed"></i> ORDERS
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link
                  eventKey="tabthree"
                  active={activeTab === 'tabthree'}
                  onClick={() => handleTabChange('tabthree')}
                  style={activeTab === 'tabthree' ? {backgroundColor: '#C55FFB', color: '#EFDCF9'} : {color: '#C55FFB'}}
                >
                  <i className="fa fa-shower"></i> USERS
                </Nav.Link>
              </Nav.Item>
            </Nav>
            <div className="tab-content mt-2">
              <div
                className={`tab-pane fade show ${activeTab === 'tabone' ? 'active' : ''} text-center`}
                id="tabone"
                role="tabpanel"
              >
                <h1 className='fw-bold mt-4'>Products Setup</h1>
                <AdminProductSetup />
              </div>
              <div
                className={`tab-pane fade ${activeTab === 'tabtwo' ? 'show active' : ''} text-center`}
                id="tabtwo"
                role="tabpanel"
              >
                <h1 className='fw-bold mt-4'>Orders Setup</h1>
                <AdminOrderSetup />
              </div>
              <div
                className={`tab-pane fade ${activeTab === 'tabthree' ? 'show active' : ''} text-center`}
                id="tabthree"
                role="tabpanel"
              >
                <h1 className='fw-bold mt-4'>Users Setup</h1>
                <AdminUserSetup />
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default AdminDashboard;
