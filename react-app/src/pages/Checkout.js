import { Container, Row, Col, Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import { useState, useEffect } from 'react';

import Swal2 from 'sweetalert2';

// Import icons 
import { BsFillArrowLeftCircleFill } from 'react-icons/bs';

// Import pages
import CheckoutCard from '../components/CheckoutCard';
import BackButton from '../components/BackButton';

const CheckOut = () => {

	// Variable Initializations
	const navigate = useNavigate();

	// States
	const [ order, setOrder ] = useState([]);
	const [ totalAmount, setTotalAmount ] = useState(0);
	const [ dateOrdered, setDateOrdered ] = useState();
	const [ orderId, setOrderId ] = useState('');

	// Format the date
	const formattedDate = dateOrdered?.toLocaleDateString('en-US', {
		month: 'long',
		day: 'numeric',
		year: 'numeric',
	});

	// Get token in local storage
	let token = localStorage.getItem('token');

	// Get cart details
	useEffect(() => {
		// Get the orders on backend API
		fetch(`${process.env.REACT_APP_API_URL}/users/myOrders`,{
			method: 'GET',
			headers: { Authorization: `Bearer ${token}` }
		})
		.then(res => res.json())
		.then(data => {
			setTotalAmount(data[0].totalAmount);
			setOrderId(data[0]._id)
			setDateOrdered(new Date())
			// getting the first and only array of data and storing it to cart variable
			const cart = data[0].products;
			setOrder(cart.map(orderInCart => {
				return(
					<CheckoutCard key={orderInCart._id} prop={orderInCart} />
				)
			}))
		})
	}, [])

	const checkout = () => {
	  fetch(`${process.env.REACT_APP_API_URL}/purchasedOrders/checkout`, {
	    method: 'POST',
	    headers: { Authorization: `Bearer ${token}` }
	  })
	    .then(checkoutResponse => checkoutResponse.json())
	    .then(checkoutData => {
	      return fetch(`${process.env.REACT_APP_API_URL}/orders/deleteCart`, {
	        method: 'DELETE',
	        headers: { Authorization: `Bearer ${token}` }
	      })
	        .then(deleteCartResponse => deleteCartResponse.json())
	        .then(deleteCartData => {
				Swal2.fire({
					title: 'Checkout successful',
					icon: 'success',
					text: 'Thank you!'
				})
				navigate('/purchasedOrders');
	        });
	    })
	};

	// checker
	/*useEffect(() => {
		console.log(dateOrdered);
	}, [dateOrdered])*/

	return (
		<div className="py-5 mt-5">
			<Container>
				<Row>
					<Col lg={12}>
				   		<BackButton />
					</Col>
					<Col className="text-center mx-auto">
		        		<h1 className="mb-4 pageTitle">Checkout Your Creativity</h1>
					</Col>
				</Row>
				<Row className="d-flex justify-content-around">
					<Col lg={7} className="px-4 py-5 bg-light d-flex flex-column mb-3">
				        <h3 className="pb-3 border-bottom border-dark">{formattedDate}</h3>
				        <h3><b>Total Price: &#8369; {totalAmount.toFixed(2)}</b></h3>
				        <p className="pb-3 border-bottom border-dark">Order ID: {orderId}</p>
				        <Row className="d-flex justify-content-between border-bottom">
							<Col className="mb-1 fw-bold">Quantity</Col>
							<Col className="mb-1 fw-bold">Item</Col>
							<Col className="mb-1 fw-bold">Price</Col>
							<Col className="mb-1 fw-bold">Total Amount</Col>
						</Row>
						{order}
        				<Button className="mt-3 bg-success" disabled={true}>Pay Now</Button>
		        		<Button className="mt-1" onClick={checkout}>Place Order</Button>
					</Col>
				</Row>
			</Container>
		</div>
	);
};

export default CheckOut;