import { Container, Row, Col, Carousel, img, Button, Form } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { useNavigate, Link } from 'react-router-dom';


// Import Component
import CartCard from '../components/CartCard';
import BackButton from '../components/BackButton';
import Products from './Products'


const Cart = () => {

	// States
	const [ cart, setCart ] = useState([]);
	const [ totalAmount, setTotalAmount ] = useState('');
	const [ error, setError ] = useState(''); 
	const [ isError, setIsError ] = useState('');

	// Get token in local storage
	let token = localStorage.getItem('token');

	// Refreshing cart function
	const refreshCart = () => {
		// Get all orders
		fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
			headers: { Authorization: `Bearer ${token}` }
		})
		.then(res => res.json())
		.then(data => {
			if ( data ) {
				// getting the first and only array of data and storing it to cart variable
				const cart = data.orderedProduct[0].products;
				if ( cart ) {
					setCart(cart.map(orderInCart => {
						return(
							<CartCard key={orderInCart._id} prop={orderInCart} refreshCart={refreshCart} />
						)
					}))
					setIsError(false)
					setTotalAmount(data.orderedProduct[0].totalAmount)
				} else {
					setError(data.orderedProduct);
					setIsError(true)
				}
				
			}
		})
	}

	// Refreshes cart every initial render
	useEffect(() => {
		refreshCart();
	}, [])

	// Checker
	/*useEffect(() => {
		console.log(totalAmount);
	}, [totalAmount])*/

	return (
	<div 
		className="py-4 mt-5"
		style={{ 
			backgroundImage: 'url("https://images.pexels.com/photos/3527786/pexels-photo-3527786.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2")', 
			backgroundPosition: 'right bottom', 
			backgroundSize: 'cover', 
			backgroundRepeat: 'repeat',
			background: 'linear-gradient(to bottom, rgba(255, 255, 255, 0.3), rgba(255, 255, 255, 0.5)), url("https://images.pexels.com/photos/3527786/pexels-photo-3527786.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2")',
			backgroundAttachment: 'fixed',
		}}
	>
		<Container className="pb-5 min-vh-100">
	 		<Row>
		      	<Col lg={12}>
		         	<BackButton />
		      	</Col>
		      	<Col lg={12} className='pageTitle text-center'>
		         	<h1 className='display-3'>Cart of Creativity</h1>
		      	</Col>
		      	{
		      		!totalAmount
		      		&&
	      			<Col lg={12} className='text-center'>
	      		   		<h2>{error}</h2>
	      		   		<h4>Check our products below</h4>
	      			</Col>
		      	}
	      	</Row>
	     		{cart}
	 	</Container>

	 	{!totalAmount && <Products />}

	 	<Container className='text-center'>

	 	{
	 		totalAmount
	 		?
	 		<div 
	 			className="
	 				py-0 
		 			fixed-bottom 
		 			d-flex flex-column 
		 			flex-md-row 
		 			justify-content-center
		 			justify-content-md-end
		 			align-items-center
		 			pe-md-5
	 			"
	 			style={{backgroundImage: 'linear-gradient(to left, #C55FFB, #EFDCF9)'}}>
		 		<div className='px-4'>
					<h2>Total Amount:  &#8369; {totalAmount}.00</h2>
				</div>
				<div className="d-inline my-2">
					<Button as={Link} to='/checkout' variant="success">Proceed to Checkout!</Button>
			 	</div>
 	 		</div>
		 	:
		 	<div 
		 		className="
		 			py-0 
		 			fixed-bottom 
		 			d-flex flex-column 
		 			flex-md-row 
		 			justify-content-center
		 			justify-content-md-end
		 			align-items-center
		 			pe-md-5
		 		"
		 		style={{backgroundImage: 'linear-gradient(to left, #C55FFB, #EFDCF9)'}}>
				<div className='px-4'>
			 		<h2>Your cart is empty</h2>
				</div>
				<div className="d-inline my-2">
	 	 	   		<Button as={Link} to='/products' variant="success">SHOP NOW!</Button>
	 	 		</div>
 	 		</div>
	 	}

	 	</Container>
 	</div>
	);
};

export default Cart;