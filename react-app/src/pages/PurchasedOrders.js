import { Container, Row, Col, Card } from 'react-bootstrap';
import { useState, useEffect } from 'react';

// Import component
import PurchasedOrderCard from '../components/PurchasedOrdersCard';

const PurchasedOrders = () => {

    // Initializations
    const [ error, setError ] = useState('');
    const [ isError, setIsError ] = useState(false);
    const [ orderHistory, setOrderHistory ] = useState([]);

    const token = localStorage.getItem('token');

    // Get all purchases
    const refreshOrders = () => {
        fetch(`${process.env.REACT_APP_API_URL}/purchasedOrders/allPurchases`, {
            headers: { Authorization: `Bearer ${token}` }
        })
        .then(res => {
            if (res.ok) {
                setIsError(false)
                return res.json();
            } else {
                res.text().then((message) => {
                    setIsError(true)
                    setError(message);
                })
            }
        })
        .then(data => {
            if (data) {
                setOrderHistory(data.map(order => (
                    <PurchasedOrderCard key={order._id} order={order} refreshOrders={refreshOrders} />
                ))
                .reverse()); // Reverse the order of the array here
            }
        })
    }

    // Refresh Orders on initial render
    useEffect(() => {
        refreshOrders();
    }, [])

    return (
        <Container className='mt-5 pt-5'>
            <Row>
                <h2 className='text-center'>Your Order History</h2>
                <Col md={12}>
                    <Card className="mt-2">
                        <Card.Body>
                            {orderHistory}
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
};

export default PurchasedOrders;