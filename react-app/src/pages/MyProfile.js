import { Container, Row, Col, Image, Card, Nav } from 'react-bootstrap';
import { useState, useContext, useEffect } from 'react';

import UserContext from '../UserContext';

import PurchasedOrders from './PurchasedOrders';


const MyProfile = () => {

	// Get user details
	const { user } = useContext(UserContext)

	// State to track the active section
	const [activeSection, setActiveSection] = useState('orderHistory');

	// Function to handle section navigation
	const handleSectionNavigation = (section) => {
		setActiveSection(section);
	};

	

	return (
	  	<Container className="py-5 mt-5">
			<Row>
				<Col md={4} className="text-center border py-5">
					<Image src='https://i1.wp.com/static.teamtreehouse.com/assets/content/default_avatar-ea7cf6abde4eec089a4e03cc925d0e893e428b2b6971b12405a9b118c837eaa2.png?ssl=1' roundedCircle fluid width={200} height={200} className="mb-3 border border-3 border-dark" />
					<h3>{user.email ? user.email : 'Loading...'}</h3>
				</Col>
				<Col md={8}>
					<Card>
						<Card.Body>
							<Card.Title className='fw-bold'>User Details</Card.Title>
							<Card.Text>
								<strong>First Name:</strong> To Follow<br />
								<strong>Last Name:</strong> To Follow<br />
								<strong>Email:</strong> {user.email}<br />
								<strong>Address:</strong> To Follow<br />
								<strong>Contact Number:</strong> To Follow
							</Card.Text>
			  	        </Card.Body>
					</Card>
					<Nav className="mt-4" fill variant="tabs" activeKey={activeSection} onSelect={handleSectionNavigation}>
		  	        	<Nav.Item>
			  	          <Nav.Link eventKey="orderHistory">Order History</Nav.Link>
			  	        </Nav.Item>
			  	        <Nav.Item>
			  	          <Nav.Link eventKey="liked">Liked Products</Nav.Link>
			  	        </Nav.Item>
					</Nav>
					{activeSection === 'orderHistory' ? (
			  	        <PurchasedOrders />
					) : (
			  	        <Card className="mt-2">
							<Card.Body>
			  	            <Card.Title>Liked Contents</Card.Title>
								<Card.Text>
									Liked Content
								</Card.Text>
							</Card.Body>
			  	        </Card>
					)}
				</Col>
			</Row>
	  	</Container>
	);
};

export default MyProfile;