import AppNavBar from './components/AppNavBar';

import './App.css';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext'

// Import pages
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import NotFound from './pages/NotFound';
import ProductView from './components/ProductView';
import Cart from './pages/Cart';
import Checkout from './pages/Checkout';
import AdminDashboard from './pages/AdminDashboard';
import MyProfile from './pages/MyProfile';
import PurchasedOrders from './pages/PurchasedOrders'
import PurchasedOrderView from './components/PurchasedOrderView'


const App = () => {

	// initialize user
	const [user, setUser] = useState({
		id: null,
		email: null,
		isAdmin: null
	});

	// clear localStorage on logout
	const unsetUser = () => {
		localStorage.clear();
	};

	// User Checker
	/*useEffect(()=> {
	    console.log(user)
	}, [user]);*/

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`,{
		    headers: {
		        Authorization: `Bearer ${localStorage.getItem('token')}`
		    }
		})
		.then(response => response.json())
		.then(data => {
		    setUser({
		        id: data._id,
		        email: data.email,
		        isAdmin: data.isAdmin
		    })
		})
	}, [])

  	return (
  		<UserProvider value = {{user, setUser, unsetUser}}>
  			<Router>
  				<AppNavBar />
  				<Container fluid style={{ padding: '0' }}>
  					<Routes>
  						<Route exact path ='/' element = {<Home/>}/>
  						<Route exact path ='/products' element = {<Products/>}/>
  						<Route exact path ='/register' element = {<Register/>}/>
  						<Route exact path ='/login' element = {<Login/>}/>
  						<Route exact path ='/logout' element = {<Logout/>}/>
  						<Route path = '*' element = {<NotFound />} />
  						<Route exact path ='/products/:id' element = {<ProductView/>}/>
  						<Route exact path ='/cart' element = {<Cart/>}/>
  						<Route exact path ='/checkout' element = {<Checkout/>}/>
  						<Route exact path ='/admin' element = {<AdminDashboard/>}/>
  						<Route exact path ='/profile' element = {<MyProfile/>}/>
  						<Route exact path ='/purchasedOrders' element = {<PurchasedOrders />}/>
  						<Route exact path ='/purchasedOrders/singlePurchase/:userId/:purchaseOrderId' element = {<PurchasedOrderView/>}/>
  					</Routes>
  				</Container>
  			</Router>
  		</UserProvider>

  	);
};

export default App;

