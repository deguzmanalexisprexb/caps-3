import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import { useState, useContext, useEffect } from 'react';

import UserContext from '../UserContext';
import Swal2 from 'sweetalert2';

import UploadWidget from '../components/UploadWidget';
import Error from '../components/Error';

const CreateProduct = () => {

	// Initializations
	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState(0);
	const [ imagesUrl, setImagesUrl ] = useState([]);
	const [ error, setError ] = useState('');
	const [ isError, setIsError ] = useState(false)

	const token = localStorage.getItem('token');

	// User context
	const{ user, setUser } = useContext(UserContext);

	const createProduct = (event) => {
		event.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/products/`, {
			method: 'POST',
			headers: {
	    		Authorization: `Bearer ${token}`,
	    		'Content-Type': 'application/json'
			},
				body: JSON.stringify({
					imagesUrl: imagesUrl,
		    		name: name,
		    		description: description,
		    		price: price
			})
		})
		.then(res => {
			if (res.ok) {
			    setIsError(false)
			    return res.json();
			} else {
			    res.text().then((message) => {
			        setIsError(true)
			        setError(message);
			    })
			}
		})
		.then(data => {
			if ( !data ) {
			    Swal2.fire({
			        title: 'Adding item unsuccessful',
			        icon: 'error',
			        text: 'Check your inputs and try again'
			    })
			} else {
			    Swal2.fire({
			        title: 'Adding item successful',
			        icon: 'success',
			        text: 'Item Added'
			    })
			    setName('');
			    setDescription('');
			    setPrice(0);
			    setImagesUrl([]);
			}
		})
	}

	// checker
	/*useEffect(() => {
		if (imagesUrl){
			console.log(imagesUrl);
		}
		
	}, [imagesUrl])*/

    return (
    	<Container>
			<Row className="justify-content-center">
				<Col md={8} lg={6} xl={4} className="mt-5">
					<div 
		    	      	className="rounded border border-3 p-4" 
		    	      	style={{ 
							backgroundImage: 'url("https://images.pexels.com/photos/3527786/pexels-photo-3527786.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2")', 
							backgroundPosition: 'right bottom', 
							backgroundSize: 'cover', 
							backgroundRepeat: 'repeat', 
							backgroundAttachment: 'fixed',
							background: 'linear-gradient(to bottom, rgba(255, 255, 255, 0.3), rgba(255, 255, 255, 0.5)), url("https://images.pexels.com/photos/3527786/pexels-photo-3527786.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2")' 
		    	      	}}
					>
						<h2 className="text-center fw-bold">Add Product</h2>
						<Form onSubmit={(e) => createProduct(e)}>
							<UploadWidget setImagesUrl={setImagesUrl} imagesUrl={imagesUrl} />
							<Form.Group controlId="userEmail">
		    	            <Form.Label className='text-white pt-3'>Product Name</Form.Label>
		    	            <Form.Control
									type="name"
									placeholder="Enter Product Name"
									value={name}
									onChange={(e) => setName(e.target.value)}
									required
		    	            />
		    	            <Error error={error} isError={isError} />
							</Form.Group>

							<Form.Group controlId="description">
		    	            <Form.Label className='text-white pt-3'>Description</Form.Label>
		    	            <Form.Control
									as="textarea"
									value={description}
									onChange={(e) => setDescription(e.target.value)}
									rows={4}
									placeholder="Enter Product Description Here!"
									required
		    	            />
							</Form.Group>

							<Form.Group controlId="price">
		    	            <Form.Label className='text-white pt-3'>Product Price</Form.Label>
		    	            <div className="input-group">
		    	              	<div className="input-group-prepend">
										<span className="input-group-text" style={{ background: 'transparent', borderRight: 0 }}>
											&#8369;
										</span>
		    	              	</div>
		    	              	<Form.Control
										type="number"
										placeholder="Enter Product Price"
										value={price}
										onChange={(e) => setPrice(e.target.value)}
										style={{ borderLeft: 0 }}
										required
		    	              	/>
		    	            </div>
							</Form.Group>

							<Button
		    	            variant="primary"
		    	            type="submit"
		    	            id="submitBtn"
		    	            className="my-3 rounded-pill border border-3 w-100"
							>
		    	            Create Product
							</Button>
						</Form>
					</div>
				</Col>
			</Row>
    	</Container>

    );
};

export default CreateProduct;