import { Accordion } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import {Table, Thead, Tbody, Tr, Th, Td} from 'react-super-responsive-table';
import 'react-super-responsive-table/dist/SuperResponsiveTableStyle.css';

const AllOrdersCard = ({ group, refresh, token }) => {
	const { userId, orders } = group;
	const [ email, setEmail ] = useState('');
	const [ productDetails, setProductDetails ] = useState([]);

	// Get the user details
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/singleUserDetails`, {
			method: 'GET',
			headers: { Authorization: `Bearer ${token}` },
		})
		.then(res => res.json())
		.then(data => {
			setEmail(data.email);
		})
	}, [userId, token]);

	// Format Date
	const formatDate = (dateString) => {
		const options = {
		    month: 'short',
		    day: 'numeric',
		    year: 'numeric',
		    hour: 'numeric',
		    minute: 'numeric',
		    hour12: true,
		};

		const formattedDate = new Date(dateString).toLocaleString('en-US', options);
		return formattedDate;
	};

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`,{
				method: 'GET',
				headers: { Authorization: `Bearer ${token}` }
			})
		.then(res => res.json())
		.then(data => {
			setProductDetails(data);
		})
	}, [])

	// Convert productId to its product name
	const convertProductId = (productId) => {
		const product = productDetails.find((product) => product._id === productId);
		if (product) {
	    	return `${product.name}`; // Assuming the product name property is 'name'
		} else {
	    	return '(deleted product)';
		}
	};
	
	// checker
	/*useEffect(() => {
		console.log(productDetails);
	}, [productDetails])*/

	return (
		<Accordion.Item eventKey={userId}>
			<Accordion.Header>Order of user: {email}</Accordion.Header>
			<Accordion.Body>
				<div className='table-responsive'>
					{orders.map(order => (
						<Table key={order._id}>
							<Thead>
								<Tr>
									<Th>Order Id</Th>
									<Th>Purchased On</Th>
									<Th>Products</Th>
									<Th>Total Amount</Th>
									<Th>Payment Status</Th>
									<Th>Delivery Status</Th>
								</Tr>
							</Thead>

							<Tbody>
								<Tr style={{ borderBottom: '1px solid #ddd' }}>
									<Td>{order._id}</Td>
									<Td>{formatDate(order.purchasedOn)}</Td>
									<Td>
							  	  		{order.products.map(product => (
							  	  			<div key={product._id}>
							  	  				<p>{product.quantity}pc/pcs {convertProductId(product.productId)} </p>
							  	  			</div>
							  	  		))}
									</Td>
									<Td>{order.totalAmount}</Td>
									<Td>{order.isPaid ? "Paid" : "Not Paid"}</Td>
									<Td>{order.isDelivered ? "Delivered" : "Not Delivered"}</Td>
								</Tr> 
							</Tbody>
						</Table>
					)).reverse()}
				</div>
			</Accordion.Body>
		</Accordion.Item>
	);
};

export default AllOrdersCard;
