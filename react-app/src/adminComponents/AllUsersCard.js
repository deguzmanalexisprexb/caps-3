import { Container, Row, Col, Carousel, img, Button, Form, Modal } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';

import Swal2 from 'sweetalert2';

const AllUsersCard = ({user, refreshUsers}) => {
	// prop passing destructuring
	const { _id: userId , email, isAdmin } = user ;

	// Get token in local storage
	const token = localStorage.getItem('token');

    // Set user as Admin
    const setAsAdmin = (userId) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/setAsAdmin`, {
            method: 'PATCH',
            headers: { Authorization : `Bearer ${token}` }
        })
        .then(res => res.json())
        .then(data => {
            Swal2.fire({
                title: 'Updating user status successful',
                icon: 'success',
                text: 'User is now a Admin'
            })
            refreshUsers();
        })
    }

    // Set admin as user
    const setAsUser = () => {
        fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/setAsUser`, {
            method: 'PATCH',
            headers: { Authorization : `Bearer ${token}` }
        })
        .then(res => res.json())
        .then(data => {
            Swal2.fire({
                title: 'Updating user status successful',
                icon: 'success',
                text: 'Admin is now a User'
            })
            refreshUsers();
        })
    }

    return (
		<Row 
            className='my-4 bg-info rounded border mx-auto' 
            style={{ 
              backgroundImage: 'url("https://images.pexels.com/photos/3527786/pexels-photo-3527786.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2")', 
              backgroundPosition: 'right bottom', 
              backgroundSize: 'cover', 
              backgroundRepeat: 'repeat', 
              backgroundAttachment: 'fixed',
              background: 'linear-gradient(to bottom, rgba(255, 255, 255, 0.3), rgba(255, 255, 255, 0.5)), url("https://images.pexels.com/photos/3527786/pexels-photo-3527786.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2")', 
              maxWidth: '800px'  
            }}
        >
			<Col lg={3} className="p-3">
				<div>
					<img 
                    	src='https://i1.wp.com/static.teamtreehouse.com/assets/content/default_avatar-ea7cf6abde4eec089a4e03cc925d0e893e428b2b6971b12405a9b118c837eaa2.png?ssl=1' 
                    	width={150} 
                    	height={150} 
                    	className="mb-3 rounded border border-3 border-dark" 
                    />
				</div>
                        
			</Col>
        	<Col lg={7} className="d-flex flex-column justify-content-center">
	         	<Form>
					<h3>{userId}</h3>
					<p>{email}</p>
						
	         		<div className="pt-2">
		         	   	{isAdmin ? <p className='text-success rounded-pill border-5 bg-info mx-5'>Admin</p> : <p className='text-danger rounded-pill border-5 bg-info mx-5'>Not Admin</p>} 
	         		</div>
	         	</Form>
        	</Col>
        	<Col lg={2} className="d-flex flex-row flex-md-column justify-content-center">

            {
                isAdmin
                ?
                <div className="d-inline-block my-2">
                    <Button variant="secondary" onClick={() => setAsUser(userId)}>Set as User</Button>
                </div>
                :
                <div className="d-inline-block my-2">
                    <Button variant="success" onClick={() => setAsAdmin(userId)}>Set as Admin</Button>
                </div>
            }

        	</Col>
     	</Row>
    );
};

export default AllUsersCard;