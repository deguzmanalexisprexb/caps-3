import { Container, Row, Col } from 'react-bootstrap';
import { useState, useEffect } from 'react';

// import components
import AllUsersCard from './AllUsersCard';

const AllUsers = ({activeTab2}) => {
	const token = localStorage.getItem('token');
	const [ allUsers, setAllUsers ] = useState([]);

	// Refresh the users info
	const refreshUsers = () => {
		// Get the orders on backend API
			fetch(`${process.env.REACT_APP_API_URL}/users/allUserDetails`,{
				method: 'GET',
				headers: { Authorization: `Bearer ${token}` }
			})
			.then(res => res.json())
			.then(data => {
				setAllUsers(data.map(user => {
					return(
						<AllUsersCard key={user._id} user={user} refreshUsers={refreshUsers} />
					)
				}))
			})
	}

	// Render when this tab is active
	useEffect(() => {
		if (activeTab2 === 'tabone') {
			refreshUsers();
		}
	},[activeTab2])

	// Checker
	useEffect(() => {
		refreshUsers();
	}, [])
    return (
		<Container className="pb-5">
	 		<Row>
		      	<Col lg={12} className='text-center'>
		         	<h1>All Users</h1>
		      	</Col>
	      	</Row>
	     		{allUsers}
	 	</Container>
    );
};

export default AllUsers;