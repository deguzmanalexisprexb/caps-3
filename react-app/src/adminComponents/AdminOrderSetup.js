import { useState } from 'react';
import { Container, Row, Col, Nav } from 'react-bootstrap';

import AllOrders from './AllOrders';

const AdminOrderSetup = () => {
	const [activeTab2, setActivetab2] = useState('tabone');

	const handleTabChange = (tab) => {
	  setActivetab2(tab);
	};

  return (
    <div className="py-3">
      <Container>
        <Row>
          <Col>
            <Nav className="nav-pills justify-content-center">
              <Nav.Item>
                <Nav.Link
                  eventKey="tabone"
                  className={activeTab2 === 'tabone' ? 'active' : ''}
                  onClick={() => handleTabChange('tabone')}
                  style={activeTab2 === 'tabone' ? {backgroundColor: '#C55FFB', color: '#EFDCF9'} : {backgroundColor: '#EFDCF9', color: '#C55FFB'}}
                >
                  All Purchased Orders
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link
                  eventKey="tabtwo"
                  className={activeTab2 === 'tabtwo' ? 'active' : ''}
                  onClick={() => handleTabChange('tabtwo')}
                  style={activeTab2 === 'tabtwo' ? {backgroundColor: '#C55FFB', color: '#EFDCF9'} : {backgroundColor: '#EFDCF9', color: '#C55FFB'}}
                >
                  Unpaid Orders
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link
                  eventKey="tabthree"
                  className={activeTab2 === 'tabthree' ? 'active' : ''}
                  onClick={() => handleTabChange('tabthree')}
                  style={activeTab2 === 'tabthree' ? {backgroundColor: '#C55FFB', color: '#EFDCF9'} : {backgroundColor: '#EFDCF9', color: '#C55FFB'}}
                >
                  Paid Orders
                </Nav.Link>
              </Nav.Item>
            </Nav>
            <div className="tab-content mt-2">
              <div
                className={`tab-pane fade show ${
                  activeTab2 === 'tabone' ? 'active' : ''
                }`}
                id="tabone"
                role="tabpanel"
              >
                <AllOrders />
              </div>
              <div
                className={`tab-pane fade ${
                  activeTab2 === 'tabtwo' ? 'show active' : ''
                }`}
                id="tabtwo"
                role="tabpanel"
              >
                <p>
                  Which was created for the bliss of souls like mine. I am so
                  happy, my dear friend, so absorbed in the exquisite.
                </p>
              </div>
              <div
                className={`tab-pane fade ${
                  activeTab2 === 'tabthree' ? 'show active' : ''
                }`}
                id="tabthree"
                role="tabpanel"
              >
                <p>
                  When I hear the buzz of the little world among the stalks, and
                  grow familiar with the countless indescribable forms.
                </p>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default AdminOrderSetup;
