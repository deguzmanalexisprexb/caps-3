import { useState } from 'react';
import { Container, Row, Col, Nav } from 'react-bootstrap';

import CreateProduct from './CreateProduct';
import AllProducts from './AllProducts';

const AdminProductSetup = () => {
	const [activeTab2, setActivetab2] = useState('tabone');

	const handleTabChange = (tab) => {
	  setActivetab2(tab);
	};

  return (
    <div className="py-3">
      <Container>
        <Row>
          <Col>
            <Nav className="nav-pills justify-content-center">
              <Nav.Item>
                <Nav.Link
                  eventKey="tabone"
                  className={activeTab2 === 'tabone' ? 'active' : ''}
                  onClick={() => handleTabChange('tabone')}
                  style={activeTab2 === 'tabone' ? {backgroundColor: '#C55FFB', color: '#EFDCF9'} : {backgroundColor: '#EFDCF9', color: '#C55FFB'}}
                >
                  Create Product
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link
                  eventKey="tabtwo"
                  className={activeTab2 === 'tabtwo' ? 'active' : ''}
                  onClick={() => handleTabChange('tabtwo')}
                  style={activeTab2 === 'tabtwo' ? {backgroundColor: '#C55FFB', color: '#EFDCF9'} : {backgroundColor: '#EFDCF9', color: '#C55FFB'}}
                >
                  All Products
                </Nav.Link>
              </Nav.Item>
            </Nav>
            <div className="tab-content mt-2">
              <div
                className={`tab-pane fade show ${
                  activeTab2 === 'tabone' ? 'active' : ''
                }`}
                id="tabone"
                role="tabpanel"
              >
                <CreateProduct />
              </div>
              <div
                className={`tab-pane fade ${
                  activeTab2 === 'tabtwo' ? 'show active' : ''
                }`}
                id="tabtwo"
                role="tabpanel"
              >
                <AllProducts activeTab2={activeTab2}/>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default AdminProductSetup;
