import { Container, Row, Col, Carousel, img, Button, Form, Modal } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';

import Swal2 from 'sweetalert2';

// Import pages
import UpdateProductModal from './UpdateProductModal';

const AllProductsCard = ({prop, refreshCart}) => {

    // Prop passing
    const { _id: productId , name, description, price, isActive, createdOn, imagesUrl } = prop;

    // Get token in local storage
    const token = localStorage.getItem('token');

    // Navigation
    const navigate = useNavigate();

    // State for modal
    const [showModal, setShowModal] = useState(false);

    const handleOpenModal = () => setShowModal(true);

    const handleCloseModal = () => setShowModal(false);

    // Archive Product
    const archiveProduct = (productId) => {
        fetch(`${process.env.REACT_APP_API_URL}/products/archive/${productId}`, {
            method: 'PATCH',
            headers: { Authorization : `Bearer ${token}` }
        })
        .then(res => res.json())
        .then(data => {
            Swal2.fire({
                title: 'Archive Product successful',
                icon: 'success',
                text: 'Archived Product'
            })
            refreshCart();
        })
    }

    // Activate Product
    const activateProduct = (productId) => {
        fetch(`${process.env.REACT_APP_API_URL}/products/activate/${productId}`, {
            method: 'PATCH',
            headers: { Authorization : `Bearer ${token}` }
        })
        .then(res => res.json())
        .then(data => {
            Swal2.fire({
                title: 'Activation Product successful',
                icon: 'success',
                text: 'Activated Product'
            })
            refreshCart();
        })
    }

    // Delete Product
    const deleteProduct = () => {
        Swal2.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                fetch(`${process.env.REACT_APP_API_URL}/products/delete/${productId}`, {
                    method: 'DELETE',
                    headers: { Authorization : `Bearer ${token}` }
                })
                .then(res => res.json())
                .then(data => {
                    if ( !data ) {
                        Swal2.fire({
                            title: 'Item Delete unsuccessful',
                            icon: 'error',
                            text: 'Check your cart and try again'
                        })
                    } else {
                        Swal2.fire({
                            title: 'Product Delete successful',
                            icon: 'success',
                            text: 'Product Deleted'
                        })
                        refreshCart();
                    }
                })
            }
        })                
    }

    return (
    			<Row 
                    className='my-4 bg-info rounded border mx-auto' 
                    style={{ 
                      backgroundImage: 'url("https://images.pexels.com/photos/3527786/pexels-photo-3527786.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2")', 
                      backgroundPosition: 'right bottom', 
                      backgroundSize: 'cover', 
                      backgroundRepeat: 'repeat', 
                      backgroundAttachment: 'fixed',
                      background: 'linear-gradient(to bottom, rgba(255, 255, 255, 0.3), rgba(255, 255, 255, 0.5)), url("https://images.pexels.com/photos/3527786/pexels-photo-3527786.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2")', 
                      maxWidth: '800px'  
                    }}
                >
    				<Col lg={3} className="p-3">
    					<Carousel id="carousel2" interval={2000}>
    						{
                                imagesUrl.map(image => (
                                    <Carousel.Item key={image.public_id}>
                                        <img className="d-block w-100" src={image.url} alt={`Slide ${image.public_id}`} />
                                    </Carousel.Item>
                                ))  
                            }
    					</Carousel>
    				</Col>
    	        	<Col lg={7} className="d-flex flex-column justify-content-center">
    	         	<Form>
    						<h1>{name}</h1>
    						<p>{description}</p>
                            <p>&#8369; {price}.00/pc</p>
    						
    	         		<div className="pt-2">
        	         	   	{isActive ? <p className='text-success rounded-pill border-5 bg-info mx-5'>Active</p> : <p className='text-danger rounded-pill border-5 bg-info mx-5'>Not Active</p>} 
    	         		</div>
    	         	</Form>
    	        	</Col>
    	        	<Col lg={2} className="d-flex flex-row flex-md-column justify-content-center">

                    {
                        isActive
                        ?
                        <div className="d-inline-block my-2">
                            <Button variant="secondary" onClick={() => archiveProduct(productId)}>Archive</Button>
                        </div>
                        :
                        <div className="d-inline-block my-2">
                            <Button variant="success" onClick={() => activateProduct(productId)}>Activate</Button>
                        </div>
                    }
                    
                        <div className="d-inline-block my-2">
                            <Button variant="info" onClick={() => setShowModal(true)}>Update</Button>
                        </div>
                        <div className="d-inline-block my-2">
                            <Button variant="danger" onClick={() => deleteProduct(productId)}>Delete</Button>
                        </div>
    	        	</Col>
                    <UpdateProductModal
                        showModal={showModal}
                        handleCloseModal={handleCloseModal}
                        product={prop}
                        token={token}
                        refreshCart={refreshCart}
                        /*updateProduct={updateProduct}*/
                    />
    	     	</Row>
    );
};

export default AllProductsCard;