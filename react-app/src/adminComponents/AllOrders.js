import { Container, Row, Col, Accordion } from 'react-bootstrap';
import { useEffect, useState } from 'react';

import AllOrdersCard from './AllOrdersCard';

const AllOrders = () => {

	const [orderGroups, setOrderGroups] = useState([]);

	const token = localStorage.getItem('token');

	// Get all users orders
	const refresh = () => {
		fetch(`${process.env.REACT_APP_API_URL}/purchasedOrders/allPurchasedOrders`, {
			headers: { Authorization: `Bearer ${token}` }
		})
		.then(res => res.json())
		.then(data => {

			// Group orders by userId
			const groupedOrders = data.reduce((result, order) => {
				const key = order.userId;
				if (!result[key]) {
					result[key] = [];
				}
				result[key].push(order);
				return result;
			}, {});
			// Convert grouped orders to an array of objects
			const orderGroups = Object.entries(groupedOrders).map(([userId, orders]) => ({
				userId,
				orders
			}));
			setOrderGroups(orderGroups);
		})
	}

	useEffect(() => {
		refresh();
	}, [])

	return (
		<Container>
			<Row>
				<Col>
					<div>
						<Accordion className='mt-5'>
							{orderGroups.map((group) => (
								<AllOrdersCard key={group.userId} group={group} refresh={refresh} token={token} />
							))}
						</Accordion>
					</div>
				</Col>
			</Row>
		</Container>
	);
};

export default AllOrders;
