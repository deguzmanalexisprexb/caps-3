import { Container, Row, Col } from 'react-bootstrap';
import { useEffect, useState } from 'react';

import AllProductsCard from './AllProductsCard';

const AllProducts = ({activeTab2}) => {
	const token = localStorage.getItem('token');
	const [ allProducts, setAllProducts ] = useState([]);

	const refreshCart = () => {
		// Get the orders on backend API
			fetch(`${process.env.REACT_APP_API_URL}/products/all`,{
				method: 'GET',
				headers: { Authorization: `Bearer ${token}` }
			})
			.then(res => res.json())
			.then(data => {
				setAllProducts(data.map(product => {
					return(
						<AllProductsCard key={product._id} prop={product} refreshCart={refreshCart} />
					)
				}))
			})
	}

	useEffect(() => {
		if (activeTab2 === 'tabtwo') {
			refreshCart();
		}
	},[activeTab2])

	useEffect(() => {
		refreshCart();
	}, [])
    return (
		<Container className="pb-5">
	 		<Row>
		      	<Col lg={12} className='text-center'>
		         	<h1>All Products</h1>
		      	</Col>
	      	</Row>
	     		{allProducts}
	 	</Container>
    );
};

export default AllProducts;